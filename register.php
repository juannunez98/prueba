<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <h1>Registrar</h1>
    <div class="container">
        <form action="./logic/save_user.php" method="POST">
            <input type="text" name="name" placeholder="Nombre completo" required>
            <input type="text" name="email" placeholder="Correo" required>
            <input type="tel" name="phone" placeholder="Teléfono" required>
            <input type="text" name="identification" placeholder="Identificación" required>
            <input type="text" name="user" placeholder="Usuario" required>
            <input type="password" name="pass" placeholder="Contraseña" required>

            <input type="submit" value="Registrar" name="register">
        </form>
    </div>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <h1>Ingresar</h1>
    <div class="container">
        <form action="./logic/log_user.php" method="POST">
            <input type="text" name="user" placeholder="Usuario" required>
            <input type="password" name="pass" placeholder="Contraseña" required>

            <input type="submit" value="Ingresar" name="login">
        </form>

        <?php
        if (isset($_SESSION['message'])) {
        ?>
            <span><?= $_SESSION['message'] ?></span>
        <?php } ?>

        <a href="register.php">Registrar</a>
    </div>
</body>

</html>
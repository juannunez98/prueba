<?php
include('./logic/database.php');

if (isset($_GET['id'])) {
    $id = (int) $_GET['id'];

    $query = "select * from casos where id = $id";

    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) === 1) {
        $row = mysqli_fetch_array($result);
        $description = $row['description'];
        $address = $row['address'];
        $id = $row['id'];
        $status = $row['status'];
    }
} ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <h1>Editar caso</h1>
    <div class="container">
        <form action="./logic/save_case.php" method="POST">
            <input type="number" name="id" value="<?= $id ?>" hidden>
            <textarea name="description" placeholder="Descripción" required><?= $description ?></textarea>
            <input type="text" name="address" placeholder="dirección" value="<?= $address ?>" required>
            <select name="status">
                <option value="0" <?= $status == 0 ? "selected" : null  ?>>inactivo</option>
                <option value="1" <?= $status == 1 ? "selected" : null  ?>>activo</option>
            </select>
            <input type="submit" value="Editar caso" name="edit_case">
        </form>
    </div>
</body>

</html>
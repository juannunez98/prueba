<?php include('./logic/database.php') ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <h1>Home</h1>
    <div class="container home-container">
        <div class="form">
            <form action="./logic/save_case.php" method="POST">
                <h2>Crear caso</h2>

                <textarea name="description" id="" cols="30" rows="10" placeholder="motivo" required></textarea>
                <input type="text" name="address" placeholder="dirección" required>

                <input type="submit" value="Guardar caso" name="save_case">
            </form>
        </div>
        <div class="list">
            <h2>Listar casos</h2>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = "Select * from casos";
                    $result = mysqli_query($conn, $query);

                    while ($row = mysqli_fetch_array($result)) {

                    ?>
                        <tr>
                            <td>
                                <?= $row['description'] ?>
                            </td>
                            <td>
                                <?= $row['address'] ?>
                            </td>
                            <td>
                                <?= $row['status'] == 0 ? "Inactivo" : "Activo" ?>
                            </td>
                            <td>
                                <a class="btn btn-sm btn-success" href="edit_case.php?id=<?= $row['id'] ?>">
                                    <i class="material-icons">edit</i>
                                </a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>